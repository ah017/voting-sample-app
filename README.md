# Simple Vote

We would like you to build a simple voting system that allows a user to vote, only once, on a
choice of 4 items. The 4 items can be fixed, and do not need to be content managed.

You should use email verification to ensure that the user is legitimate, and put into place
rules to ensure only one vote per email address can be logged. Alongside registering the
user's vote, their IP address and estimated location should also be stored.

Storing votes should be queued, as this tool is expecting high traffic.

At the end of each day 23:59, an email should be sent to dev@unifysoftware.com, with the
totals of the scores.

The user interface should be simple and clean, with the front-end built in Vue.js and
TailwindCSS (or your preferred CSS framework) and back-end build using Laravel (API with
Sanctum auth)

This project should take no longer than 4hrs, once complete, please provide a GitHub repo
for us to review.
