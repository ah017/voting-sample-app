<?php

namespace Tests\Feature;

use App\Jobs\DailyStatisticsReport;
use App\Models\Option;
use App\Models\Survey;
use App\Models\Vote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class VoteTest extends TestCase
{
    use RefreshDatabase;

    protected function createSurvey()
    {
        $survey = Survey::factory()
            ->has(Option::factory()->count(4))
            ->create();

        $survey->options->each(function (Option $option, $i) {
            Vote::factory()
                ->for($option)
                ->count(($i + 1) * 10)
                ->create();
        });

        return $survey;
    }

    public function test_create_survey_helper(): void
    {
        $survey = $this->createSurvey();

        $this->assertEquals(100, $survey->votes()->count());
    }

    public function test_daily_report_is_sent(): void
    {
        $this->createSurvey();

        Mail::fake();

        (new DailyStatisticsReport())->handle();

        Mail::assertSent(\App\Mail\DailyStatisticsReport::class);
    }
}
