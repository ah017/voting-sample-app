<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Survey extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function options(): HasMany
    {
        return $this->hasMany(Option::class);
    }

    public function votes(): HasManyThrough
    {
        return $this->hasManyThrough(
            Vote::class,
            Option::class,
            'survey_id',
            'survey_option_id',
        );
    }
}
