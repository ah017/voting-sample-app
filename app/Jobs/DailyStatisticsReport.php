<?php

namespace App\Jobs;

use App\Models\Option;
use App\Models\Survey;
use App\Models\Vote;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class DailyStatisticsReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    public function __invoke()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $survey = Survey::query()
            ->active()
            ->with('options')
            ->first();

        if (!$survey) {
            logger('No active survey to report on');
            return;
        }

        $sums = Vote::query()
            ->selectRaw('count(survey_option_id) as count, survey_option_id')
            ->whereIn('survey_option_id', $survey->options->pluck('id'))
            ->groupBy('survey_option_id');

        $results = Option::query()
            ->select('survey_options.*', 'sums.count')
            ->whereIn('survey_options.id', $survey->options->pluck('id'))
            ->leftJoinSub($sums, 'sums', function ($join) {
                $join->on('sums.survey_option_id', '=', 'survey_options.id');
            })
            ->orderBy('sums.count', 'desc')
            ->get();

        Mail::to(env('DAILY_STATISTICS_RECIPIENT'))
            ->send(new \App\Mail\DailyStatisticsReport($survey, $results));
    }
}
