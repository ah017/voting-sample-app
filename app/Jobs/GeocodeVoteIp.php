<?php

namespace App\Jobs;

use App\Models\Vote;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\GeocodeIp\Contracts\GeocodeIpInterface;

class GeocodeVoteIp implements ShouldQueue, ShouldBeUnique
{
    use
        Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public Vote $vote;

    public function __construct(Vote $vote)
    {
        $this->vote = $vote;
    }

    public function handle(GeocodeIpInterface $geocodeIp): void
    {
        $latlng = $geocodeIp->ipAddressToLatLng($this->vote->ip_address);

        $this->vote->update([
            'latitude' => $latlng->latitude,
            'longitude' => $latlng->longitude,
        ]);
    }

    public function uniqueId()
    {
        return $this->vote->id;
    }
}
