<?php

namespace App\Jobs;

use App\Models\Answer;
use App\Models\Option;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveVote implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public User $user;
    public Option $option;
    public string $ip_address;

    public function __construct(User $user, Option $option, string $ip_address)
    {
        $this->user = $user;
        $this->option = $option;
        $this->ip_address = $ip_address;
    }

    public function handle(): void
    {
        $survey = $this->option->survey;

        if (!$survey) {
            logger('Survey not found');
            return;
        }

        if (!$survey->is_active) {
            logger('Survey is not active');
            return;
        }

        $vote = $survey->votes()->where('user_id', $this->user->id)->firstOrNew();
//
        if ($vote->exists) {
            logger('User has already voted');
            // @TODO - allow changing of their vote?
//            return;
        }

        $vote->user()->associate($this->user);
        $vote->option()->associate($this->option);
        $vote->ip_address = $this->ip_address;
        $vote->save();

        dispatch(new GeocodeVoteIp($vote));
    }
}
