<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function index()
    {
        return Survey::query()
            ->active()
            ->with('options')
            ->firstOrFail();
    }
}
