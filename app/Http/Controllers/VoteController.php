<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVoteRequest;
use App\Jobs\SaveVote;
use App\Models\Option;
use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class VoteController extends Controller
{
    public function store(Survey $survey, StoreVoteRequest $request)
    {
        $option = Option::findOrFail($request->option);

        dispatch(new SaveVote($request->user(), $option, $request->ip()));

        return response(null, SymfonyResponse::HTTP_ACCEPTED);
    }
}
