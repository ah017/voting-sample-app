<?php

namespace App\Http\Requests;

use App\Models\Option;
use App\Models\Survey;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreVoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'option' => [
                'required',
                'integer',
                Rule::exists(Option::class, 'id')
                    ->where('survey_id', optional($this->survey)->id ?: 0),
                Rule::prohibitedIf(fn () => !optional($this->survey)->is_active),
            ],
        ];
    }
}
