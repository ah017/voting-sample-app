<?php

namespace App\Console\Commands;

use App\Models\Option;
use App\Models\Survey;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateNewSurvey extends Command
{
    protected $signature = 'app:create-new-survey';

    public function handle()
    {
        DB::beginTransaction();

        Survey::active()->update([
            'is_active' => 0,
        ]);

        $survey = Survey::forceCreate([
            'title' => 'New Survey ' . now()->format('d/m/Y H:i:s'),
            'is_active' => 1,
        ]);

        foreach (range(1, 4) as $id) {
            $survey->options()->save(new Option([
                'title' => 'Option #' . $id . ' (' . fake()->words(1, true) . ')',
            ]));
        }

        DB::commit();
    }
}
