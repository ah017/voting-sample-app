<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('survey', \App\Http\Controllers\SurveyController::class)
        ->only(['index']);

    Route::apiResource('survey.vote', \App\Http\Controllers\VoteController::class)
        ->only(['store']);
});
