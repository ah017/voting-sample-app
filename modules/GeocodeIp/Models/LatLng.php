<?php

namespace Modules\GeocodeIp\Models;

use Modules\GeocodeIp\Contracts\LatLngInterface;

class LatLng implements LatLngInterface
{
    public float $latitude;
    public float $longitude;

    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }
}
