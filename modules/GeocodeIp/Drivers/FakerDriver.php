<?php

namespace Modules\GeocodeIp\Drivers;

use Faker\Generator;
use Modules\GeocodeIp\Contracts\GeocodeIpInterface;
use Modules\GeocodeIp\Contracts\LatLngInterface;
use Modules\GeocodeIp\Models\LatLng;

class FakerDriver implements GeocodeIpInterface
{
    protected Generator $faker;

    public function __construct()
    {
        $this->faker = fake();
    }

    public function ipAddressToLatLng(string $ip_address): LatLngInterface
    {
        return new LatLng(
            $this->faker->latitude(),
            $this->faker->longitude(),
        );
    }
}
