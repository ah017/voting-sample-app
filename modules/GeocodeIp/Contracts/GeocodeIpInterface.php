<?php

namespace Modules\GeocodeIp\Contracts;

interface GeocodeIpInterface
{
    public function ipAddressToLatLng(string $ip_address): LatLngInterface;
}
