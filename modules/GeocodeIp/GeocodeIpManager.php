<?php

namespace Modules\GeocodeIp;

use Illuminate\Support\Manager;
use Modules\GeocodeIp\Contracts\GeocodeIpInterface;
use Modules\GeocodeIp\Drivers\FakerDriver;
use Modules\GeocodeIp\Providers\ServiceProvider;

class GeocodeIpManager extends Manager
{
    public function getDefaultDriver(): string
    {
        return config(ServiceProvider::MODULE . 'default') ?: 'faker';
    }

    public function createFakerDriver(): GeocodeIpInterface
    {
        return new FakerDriver();
    }
}
