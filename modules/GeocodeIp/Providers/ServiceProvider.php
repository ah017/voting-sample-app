<?php

namespace Modules\GeocodeIp\Providers;

use Modules\GeocodeIp\Contracts\GeocodeIpInterface;
use Modules\GeocodeIp\GeocodeIpManager;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public const MODULE = 'geocode-ip';

    public function register(): void
    {
        $this->app->singleton(static::MODULE, function ($app) {
            return new GeocodeIpManager($app);
        });

        $this->app->singleton(static::MODULE . '.driver', function ($app) {
            return $app[static::MODULE]->driver();
        });

        $this->app->alias(static::MODULE . '.driver', GeocodeIpInterface::class);
    }
}
