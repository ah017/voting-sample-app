<x-mail::message>
# Here's the daily stats for the current survey

## {{ $survey->title }}

<x-mail::table :url="''">
| Option | Vote Count |
| ------ |:----------:|
@foreach ($statistics as $option)
| {{ $option->title }} | {{ $option->count ?: 0 }} |
@endforeach
</x-mail::table>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
