<?php

return [
    App\Providers\AppServiceProvider::class,
    Modules\GeocodeIp\Providers\ServiceProvider::class,
];
